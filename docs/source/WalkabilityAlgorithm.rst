Inline-Documentation of WalkieProcessingAlgorithm
=================================================

.. note::
   Version |release|

.. class:: WalkieProcessingAlgorithm

.. todo::
   try to fix automodule!

..
	.. automodule:: os_walk_eu_plugin.algorithm.walkability_algorithm.WalkieProcessingAlgorithm
	   :members:
	   :private-members:
	   :special-members: __init__
	   :undoc-members:
	   