
from qgis.PyQt.QtCore import QCoreApplication


def tr(string):
    """
    Returns a translatable string with the self.tr() function.
    """
    return QCoreApplication.translate('Processing', string)





def shortHelpString():
    """
    Returns a localised short helper string for the algorithm. This string
    should provide a basic description about what the algorithm does and the
    parameters and outputs associated with it..
    """
    return tr(""      
        
            "The OS-WALK-EU plugin calculates the walkability on the neighborhood level. "
            "The selection of the initial geometry is left to the user. However, care should "
            "be taken to ensure that the geometries (polygon) are of an appropriate size. We "
            "recommend the 500 x 500 m Inspire Grid. The algorithm accepts common formats " 
            "(shapefile, feature, geojson). For the first applications, you can select pre-selected "
            "pedestrian profiles based on age group and socio-economic status. For more information " 
            "on the design of the profiles: https://gitlab.com/ils-research/os-walk-eu/-/wikis/Predefined-profiles. "
            "The provision of elevation data is optional. If your study area has considerable slopes that affects "
            "walkability, we recommend to use a digital elevation model (DEM) as input. The data can be sourced from"
            "your own local data or e.g., from Copernicus. Select the output folder to set the save location. This "
            "automatically creates a gpkg file containing the final result. In addition, all generated and downloaded "
            "files are stored in a ‘tempdir’ folder, which can be helpful for interpreting the results. If this is "
            "not needed, the folder can be deleted. If you want to set the parameters for the calculation yourself"
            " instead of using the predefined profiles, go to the expert settings. Activate the button to set the "
            "different weightings. It is also possible to include your own data sets in the amenity analysis by "
            "activating the button and selecting the desired data set. For more information and details about the "
            "algorithm go to https://gitlab.com/ils-research/os-walk-eu/-/wikis/home. "

              )
